package com.gabchak.model;

public class Client {

    private String name;
    private String phoneNumber;
    private String address;

    public Client(String name, String phoneNumber, String address) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "[Name='" + name + '\''
                + ", Phone_number='" + phoneNumber
                + ", Address='" + address + "]";
    }
}
