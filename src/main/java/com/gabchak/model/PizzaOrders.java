package com.gabchak.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class PizzaOrders {

    private Map<Long, Order> orders;

    public PizzaOrders() {
        orders = new LinkedHashMap<>();
    }

    public Map<Long, Order> getOrders() {
        return orders;
    }

    public void addOrder(Order order) {
        this.orders.put(order.getOrderId(), order);
    }

    public void deleteOrderByID(long orderID) {
        orders.remove(orderID);
    }

    @Override
    public String toString() {
        return "Pizza_orders:\n" + orders.values();
    }
}
