package com.gabchak.model;

public class Pizza {

    private String pizzaName;

    public Pizza(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    @Override
    public String toString() {
        return "Pizza_name='" + pizzaName + "'";
    }

}