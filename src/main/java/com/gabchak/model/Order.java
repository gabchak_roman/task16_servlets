package com.gabchak.model;

import java.util.List;

public class Order {

    private static int unique = 0;
    private Client client;
    private List<Pizza> pizzaList;
    private long id;

    public Order(Client client, List<Pizza> pizzas) {
        this.client = client;
        this.pizzaList = pizzas;
        id = ++unique;
    }

    public long getOrderId() {
        return id;
    }

    public Client getClientInfo() {
        return client;
    }

    public List<Pizza> getPizzaList() {
        return pizzaList;
    }

    @Override
    public String toString() {
        return    "\nOrder ID: " + id + "\n"
                + "Client - " + client + "\n"
                + "Pizzas - " + pizzaList +"\n";
    }
}
