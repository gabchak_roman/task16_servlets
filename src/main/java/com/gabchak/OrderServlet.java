package com.gabchak;

import com.gabchak.model.Client;
import com.gabchak.model.Order;
import com.gabchak.model.Pizza;
import com.gabchak.model.PizzaOrders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/orders/*")
public class OrderServlet extends HttpServlet {

    private static Logger log = LogManager.getLogger(OrderServlet.class);
    private static PizzaOrders orders = new PizzaOrders();

    @Override
    public void init() {
        log.info("Servlet is initialized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setAttribute("orders", orders.getOrders());
        req.getRequestDispatcher("orders.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        String clientName = req.getParameter("client_name");
        String clientPhone = req.getParameter("client_phone");
        String clientAddress = req.getParameter("client_address");
        String pizzaName = req.getParameter("pizza_name");
        List<Pizza> pizzaList = new ArrayList<>();
        pizzaList.add(new Pizza(pizzaName));

        if (!clientName.isEmpty() && !clientPhone.isEmpty() && !clientAddress.isEmpty() && !pizzaName.isEmpty()) {
            orders.addOrder(
                    new Order(
                            new Client(clientName, clientPhone, clientAddress),
                            pizzaList));
        }

        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
        orders.deleteOrderByID(Integer.parseInt(
                req.getRequestURI().replace("/orders/", "")));
    }

    @Override
    public void destroy() {
        log.info("Servlet is closed");
        super.destroy();
    }

}
