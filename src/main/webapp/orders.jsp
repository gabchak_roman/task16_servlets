<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
</head>
<body>
<h1>Make order</h1>
<form action='orders' method='POST'>
    <h3>Client</h3>
    <p>name:</p>
    <p><input name='client_name' type='text'>
    <p>phone number:</p>
    <p><input name='client_phone' type='text'>
    <p>address:</p>
    <p><input name='client_address' type='text'>
    <p><h3>Pizza name:<h3>
    <p><input name='pizza_name' type='text'>
    <p><button type='submit'>Order pizza</button>
</form>

<form>
    <h3><p>Delete order:</p></h3>
    <p> Order id: <input name='order_id' type='text'>
    <p><input name='ok' onclick='remove(this.form.order_id.value)' type='button' value='Delete Order'/>
    <p>
    </p>
</form>

<script type='text/javascript'>
  function remove(id) {
    fetch('orders/' + id, {method: 'DELETE'})
        .then( function(response) {
            location.replace("/orders");
        });
  }
</script>

<table>
  <TH>Id</th>
  <TH>Order</th>
  <c:forEach items="${orders}" var="enrty">
    <tr>
      <td><c:out value="${enrty.key}" /><td>
      <td><c:out value="${enrty.value}" /><td>
    </tr>
  </c:forEach>
</table>

</body>
</html>